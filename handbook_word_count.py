import re, glob, os, time
from wordcloud import WordCloud, STOPWORDS

stopwords = set(STOPWORDS)
stopwords.update(["gitlab", "https", "content", "handbook", "will", "use", "user", "using", "project", "org",
                   "ce", "work", "need", "change", "merge_requests", "png"])
text = ''

for filename in glob.iglob('./www-gitlab-com/source/**', recursive=True):
    if os.path.isfile(filename) and re.search(r'.md$', filename) : # keep only .md files
        text = text + open(filename).read().lower()

wordcloud = WordCloud(max_words=100, stopwords=stopwords, width=1000, height=1000, background_color="white").generate(text)
wordcloud.to_file('./output/' + time.strftime("%Y-%m-%d_%H-%M-%S") + '.png')
